using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.Runtime;
using System.IO;
using System;
using Amazon.S3.Util;
using System.Collections.Generic;
using Amazon.CognitoIdentity;
using Amazon;
using TMPro;

namespace AWSSDK.Examples
{
	public class S3CloudTileManager : MonoBehaviour
	{
		public string IdentityPoolId = "";
		public string CognitoIdentityRegion = RegionEndpoint.USEast2.SystemName;
		private RegionEndpoint _CognitoIdentityRegion
		{
			get { return RegionEndpoint.GetBySystemName(CognitoIdentityRegion); }
		}
		public string S3Region = RegionEndpoint.USEast2.SystemName;
		private RegionEndpoint _S3Region
		{
			get { return RegionEndpoint.GetBySystemName(S3Region); }
		}
		public string S3BucketName = null;
		public string SampleFileName = null;
		int count = 0;

        [SerializeField]
        Transform[] wallTileListContents;

        [SerializeField]
        GameObject tilePrefab;

        [SerializeField]
        List<GameObject> cloudTileObjects = new List<GameObject>();

        [SerializeField]
        List<WallDetails> wallDetailList;

        [SerializeField]
        Shader tileMatShader;

        void Start()
		{
			UnityInitializer.AttachToGameObject(this.gameObject);
			AWSConfigs.HttpClient = AWSConfigs.HttpClientOption.UnityWebRequest;

			AWSConfigs.LoggingConfig.LogTo = LoggingOptions.UnityLogger;
			AWSConfigs.LoggingConfig.LogResponses = ResponseLoggingOption.Always;
			AWSConfigs.LoggingConfig.LogMetrics = true;
			AWSConfigs.CorrectForClockSkew = true;                        
            GetObjects();
        }

		#region private members

		private IAmazonS3 _s3Client;
		private AWSCredentials _credentials;

		private AWSCredentials Credentials
		{
			get
			{
				if (_credentials == null)
					_credentials = new CognitoAWSCredentials(IdentityPoolId, _CognitoIdentityRegion);
				Debug.Log("Got Credentials : " + _credentials);
				return _credentials;
			}
		}

        private IAmazonS3 Client
		{
			get
			{
				if (_s3Client == null)
				{
					Debug.Log("Client Call : " + Credentials + " : " + _S3Region + " : " + _s3Client);
					_s3Client = new AmazonS3Client(Credentials, _S3Region);
				}
				//test comment
				return _s3Client;
			}
		}

		#endregion

		/// <summary>
		/// Get Object from S3 Bucket
		/// </summary>
		private void GetObject(int count = 0, string fileName = "")
		{
            //ResultText.text = string.Format("fetching {0} from bucket {1}", SampleFileName, S3BucketName);
            Client.GetObjectAsync(S3BucketName, SampleFileName, (responseObj) =>
			{
				byte[] data = null;
				var response = responseObj.Response;
				Stream input = response.ResponseStream;

                Debug.Log("Tile Object : " + cloudTileObjects);

                StartCoroutine(ConvertResponseToTexture(input, fileName));
                return;

                if (response.ResponseStream != null)
				{
					byte[] buffer = new byte[16 * 1024];
					using (MemoryStream ms = new MemoryStream())
					{
						int read;
						while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
						{
							ms.Write(buffer, 0, read);
						}
						data = ms.ToArray();
					}

                    //if (count < displayTexture.Length)
                    //{
                    //	//Display Image
                    //	displayTexture[count].texture = bytesToTexture2D(data);
                    //}
                    //else
                    //{
                    //	Debug.Log("No slots to add this texture " + count);
                    //}
                    
                    tilePrefab.GetComponentInChildren<RawImage>().texture = bytesToTexture2D(data);
                    
                    string[] actualName = fileName.Split('.');
                    tilePrefab.GetComponentInChildren<TextMeshProUGUI>().text = actualName[0];
                    
                }
			});
		}

        IEnumerator ConvertResponseToTexture(Stream input, string fileName)
        {
            yield return null;
            byte[] data = null;
            if (input != null)
            {
                byte[] buffer = new byte[16 * 1024];
                using (MemoryStream ms = new MemoryStream())
                {
                    int read;
                    while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        ms.Write(buffer, 0, read);
                        yield return null;
                    }
                    data = ms.ToArray();
                }

                yield return null;
                tilePrefab.GetComponentInChildren<RawImage>().texture = bytesToTexture2D(data);

                // Creating new Mat
                yield return null;
                Material material = new Material(tileMatShader);
                material.mainTexture = tilePrefab.GetComponentInChildren<RawImage>().texture;

                // Creating new Walldetails object
                yield return null;
                WallDetails wallDetailsObj = ScriptableObject.CreateInstance<WallDetails>();
                wallDetailsObj.materials = new Material[1];
                wallDetailsObj.materials[0] = material;
                wallDetailsObj.style = WallDetails.Style.Grid;
                //wallDetailsObj.isRandom = true;
                wallDetailsObj.wallHeight = 3.34f;
                wallDetailsObj.wallWidth = 5.41f;

                

                wallDetailList.Add(wallDetailsObj);
                yield return null;
                string[] actualName = fileName.Split('.');
                tilePrefab.GetComponentInChildren<TextMeshProUGUI>().text = actualName[0];

                foreach (Transform wallTileListContent in wallTileListContents)
                {
                    Debug.Log("Instantiate : " + wallTileListContent.transform.root.name);
                    GameObject currentTile = Instantiate(tilePrefab, wallTileListContent) as GameObject;
                    cloudTileObjects.Add(currentTile);
                    if (wallTileListContent.transform.name.Equals("WallPatternListContent"))
                    {
                        currentTile.GetComponent<Button>().onClick.AddListener(() => wallTileListContent.transform.root.GetComponent<WallUI>().OnPatternPressed(wallDetailsObj));
                    }
                    else
                    {
                        currentTile.GetComponent<Button>().onClick.AddListener(() => wallTileListContent.transform.root.GetComponent<WallUI>().OnTilePressed(wallDetailsObj));
                    }
                    
                }
            }

        }

        /// <summary>
        /// Get Objects from S3 Bucket
        /// </summary>
        public void GetObjects()
		{
			//ResultText.text = "Fetching all the Objects from " + S3BucketName;

			var request = new ListObjectsRequest()
			{
				BucketName = S3BucketName
			};

			Client.ListObjectsAsync(request, (responseObject) =>
			{
				//ResultText.text += "\n";
				if (responseObject.Exception == null)
				{
					//ResultText.text += "Got Response \nPrinting now \n";
					responseObject.Response.S3Objects.ForEach((o) =>
					{
						SampleFileName = o.Key;
                        GetObject(count, SampleFileName);                       
						//ResultText.text += string.Format("{0}\n", o.Key + " " + o.GetType() + " " + o.Size);
					});
				}
				else
				{
					//ResultText.text += "Got Exception \n";
				}
			});
		}

        public Texture2D bytesToTexture2D(byte[] imageBytes)
        {
            Texture2D tex = new Texture2D(2, 2);
            tex.LoadImage(imageBytes);
            return tex;
        }

        #region helper methods

        private string GetFileHelper()
		{
			var fileName = SampleFileName;

			if (!File.Exists(Application.persistentDataPath + Path.DirectorySeparatorChar + fileName))
			{
				var streamReader = File.CreateText(Application.persistentDataPath + Path.DirectorySeparatorChar + fileName);
				streamReader.WriteLine("This is a sample s3 file uploaded from unity s3 sample");
				streamReader.Close();
			}
			return fileName;
		}

		private string GetPostPolicy(string bucketName, string key, string contentType)
		{
			bucketName = bucketName.Trim();

			key = key.Trim();
			// uploadFileName cannot start with /
			if (!string.IsNullOrEmpty(key) && key[0] == '/')
			{
				throw new ArgumentException("uploadFileName cannot start with / ");
			}

			contentType = contentType.Trim();

			if (string.IsNullOrEmpty(bucketName))
			{
				throw new ArgumentException("bucketName cannot be null or empty. It's required to build post policy");
			}
			if (string.IsNullOrEmpty(key))
			{
				throw new ArgumentException("uploadFileName cannot be null or empty. It's required to build post policy");
			}
			if (string.IsNullOrEmpty(contentType))
			{
				throw new ArgumentException("contentType cannot be null or empty. It's required to build post policy");
			}

			string policyString = null;
			int position = key.LastIndexOf('/');
			if (position == -1)
			{
				policyString = "{\"expiration\": \"" + DateTime.UtcNow.AddHours(24).ToString("yyyy-MM-ddTHH:mm:ssZ") + "\",\"conditions\": [{\"bucket\": \"" +
					bucketName + "\"},[\"starts-with\", \"$key\", \"" + "\"],{\"acl\": \"private\"},[\"eq\", \"$Content-Type\", " + "\"" + contentType + "\"" + "]]}";
			}
			else
			{
				policyString = "{\"expiration\": \"" + DateTime.UtcNow.AddHours(24).ToString("yyyy-MM-ddTHH:mm:ssZ") + "\",\"conditions\": [{\"bucket\": \"" +
					bucketName + "\"},[\"starts-with\", \"$key\", \"" + key.Substring(0, position) + "/\"],{\"acl\": \"private\"},[\"eq\", \"$Content-Type\", " + "\"" + contentType + "\"" + "]]}";
			}

			return policyString;
		}

	}

	#endregion
}
