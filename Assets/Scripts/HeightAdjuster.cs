﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeightAdjuster : MonoBehaviour
{
    [SerializeField]
    float movementPoint = 0.01f;

    private void Update()
    {
        Vector2 primaryTouchpad = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad);
        if (OVRInput.GetDown(OVRInput.Button.PrimaryTouchpad) && ((primaryTouchpad.x > -0.40f || primaryTouchpad.x < 0.40f) && primaryTouchpad.y >= 0.50f) || Input.GetKeyDown(KeyCode.S))
        {
            // UP Movement
            transform.position += new Vector3(0f, movementPoint, 0f);
        }
        else if (OVRInput.GetDown(OVRInput.Button.PrimaryTouchpad) && ((primaryTouchpad.x > -0.40f || primaryTouchpad.x < 0.40f) && primaryTouchpad.y <= -0.50f) || Input.GetKeyDown(KeyCode.X))
        {
            // Down Movement
            transform.position -= new Vector3(0f, movementPoint, 0f);
        }
    }
}
