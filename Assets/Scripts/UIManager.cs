﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    Button  room, bathroom1, bathroom2, bathroom3;

    [SerializeField]
    GameObject loader;

    public static float humanHeightSelected;

    [SerializeField]
    Transform humanHeightPoint;

    private void Awake()
    {
        if (SceneManager.GetActiveScene().name.Equals("MainSceneTest"))
        {
            room.onClick.AddListener(RoomClicked);
            bathroom1.onClick.AddListener(Bathroom1Clicked);
            bathroom2.onClick.AddListener(Bathroom2Clicked);
            bathroom3.onClick.AddListener(Bathroom3Clicked);
            humanHeightSelected = 1.58496f;
        }else
        {
            if(humanHeightSelected != 0.0f)
                humanHeightPoint.position = new Vector3(humanHeightPoint.position.x, humanHeightSelected, humanHeightPoint.position.z);
        }
    }

    private void Update()
    {
        /*
        if (Input.GetKeyDown(KeyCode.G))
        {
            RoomClicked();
        }else if (Input.GetKeyDown(KeyCode.D))
        {
            Bathroom1Clicked();
        }
        else if (Input.GetKeyDown(KeyCode.C))
        {
            Bathroom2Clicked();
        }
        else if (Input.GetKeyDown(KeyCode.B))
        {
            Bathroom3Clicked();
        }
        */

        if (!SceneManager.GetActiveScene().name.Equals("MainSceneTest"))
        {
            if (Input.GetKeyDown(KeyCode.Escape) || OVRInput.GetDown(OVRInput.Button.Two))
            {
                loader.SetActive(true);
                Invoke("MovetoMainScene", 0.5f);
            }
        }
    }



    public void MovetoMainScene()
    {
        SceneManager.LoadScene("MainSceneTest");
    }

    public void RoomClicked()
    {
        SceneManager.LoadSceneAsync("Johnson_Room_VR_Multiplayer", LoadSceneMode.Single);
        loader.SetActive(true);
        bathroom1.enabled = false;
        bathroom2.enabled = false;
        bathroom3.enabled = false;
        room.enabled = false;
    }

    public void Bathroom1Clicked()
    {
        SceneManager.LoadSceneAsync("BathroomTile", LoadSceneMode.Single);        
        loader.SetActive(true);
        bathroom1.enabled = false;
        bathroom2.enabled = false;
        bathroom3.enabled = false;
        room.enabled = false;
    }

    public void Bathroom2Clicked()
    {
        //SceneManager.LoadSceneAsync("BathroomTile2", LoadSceneMode.Single);      
        SceneManager.LoadSceneAsync("BathroomTile2", LoadSceneMode.Single);
        loader.SetActive(true);
        bathroom1.enabled = false;
        bathroom2.enabled = false;
        bathroom3.enabled = false;
        room.enabled = false;
    }

    public void Bathroom3Clicked()
    {
        SceneManager.LoadSceneAsync("BathroomTile3", LoadSceneMode.Single);
        loader.SetActive(true);
        bathroom1.enabled = false;
        bathroom2.enabled = false;
        bathroom3.enabled = false;
        room.enabled = false;
    }

    public void HeightValueChanged(float heightVal)
    {
        humanHeightSelected = heightVal;
    }

}
