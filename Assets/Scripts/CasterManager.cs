﻿using UnityEngine;

public class CasterManager : MonoBehaviour
{
    [SerializeField]
    Transform oculusCamera;

    [SerializeField]
    GameObject bezierVisualizer, gazePointer, gazeRectile;
    /*private void Update()
    {
        Vector2 primaryTouchpad = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad);

        if ((OVRInput.GetDown(OVRInput.Button.PrimaryTouchpad) && ((primaryTouchpad.x <= -0.50f) && (primaryTouchpad.y > -0.40f || primaryTouchpad.y < 0.40f))) || Input.GetKeyDown(KeyCode.L))
        {
            // Left Movement
            //debugUIText.text = "Left Movement";
            Debug.Log("Left Movement" + oculusCamera.rotation);
            oculusCamera.Rotate(0f, -10f, 0f);
            return;
        }
        else if ((OVRInput.GetDown(OVRInput.Button.PrimaryTouchpad) && ((primaryTouchpad.x >= 0.50f) && (primaryTouchpad.y > -0.40f || primaryTouchpad.y < 0.40f))) || Input.GetKeyDown(KeyCode.R))
        {
            // Right Movement
            //debugUIText.text = "Right Movement";
            Debug.Log("Right Movement" + oculusCamera.rotation);
            oculusCamera.Rotate(0f, +10f, 0f);
            return;
        }

        if (OVRInput.GetDown(OVRInput.Button.PrimaryTouchpad) || Input.GetKeyDown(KeyCode.A))
        {
            Debug.Log("Touch Pad down");
            bezierVisualizer.SetActive(true);
            gazePointer.SetActive(false);
            gazeRectile.SetActive(false);
        }else if (OVRInput.GetUp(OVRInput.Button.PrimaryTouchpad) || Input.GetKeyUp(KeyCode.A)){
            Debug.Log("Touch Pad up");
            Invoke("EnableGaze", 0.2f);
            //EnableGaze();
        }
    }*/

    private void Update()
    {
        if (OVRInput.GetDown(OVRInput.Button.SecondaryThumbstickUp) || Input.GetKeyDown(KeyCode.A))
        {
            Debug.Log("Touch Pad down");
            ///bezierVisualizer.SetActive(true);
            gazePointer.SetActive(false);
            gazeRectile.SetActive(false);
        }
        else if (OVRInput.GetUp(OVRInput.Button.SecondaryThumbstickUp) || Input.GetKeyUp(KeyCode.A))
        {
            Debug.Log("Touch Pad up");
            Invoke("EnableGaze", 0.2f);
            //EnableGaze();
        }
    }


    void EnableGaze()
    {
        //bezierVisualizer.SetActive(false);
        gazePointer.SetActive(true);
        gazeRectile.SetActive(true);
    }
}
