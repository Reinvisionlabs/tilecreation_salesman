﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureContainer : MonoBehaviour
{
    public static TextureContainer textureContainer;

    public List<WallDetails> wallDet_1_3;
    public List<WallDetails> wallDet_2_4;

    private void Awake()
    {
        //wallDet_1_3 = new List<WallDetails>();
        //wallDet_2_4 = new List<WallDetails>();

        textureContainer = this;
    }


    public WallDetails GetWallDetails(int wallNum, int indexVal)
    {
        Debug.Log("inside GetWallDetails");
        if (wallNum == 1 || wallNum == 3)
            return wallDet_1_3[indexVal];

        else
            return wallDet_2_4[indexVal];
    }

}
