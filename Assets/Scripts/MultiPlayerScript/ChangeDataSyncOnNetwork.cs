﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

[System.Serializable]
public class PaintSyncDataFormat
{
    public bool isTexture = false;
    public int wallNum;
    public float r, g, b;
    public int textureIndex;
}

public class ChangeDataSyncOnNetwork : MonoBehaviour
{
    public static ChangeDataSyncOnNetwork instance;
    public List<PaintSyncDataFormat> latestWallcolorList;
    PhotonView PV;

    private void Awake()
    {
        instance = this;
        latestWallcolorList = new List<PaintSyncDataFormat>();
        PV = GetComponent<PhotonView>();
        for (int i = 0; i < 4; i++)
        {
            PaintSyncDataFormat wall = new PaintSyncDataFormat();
            wall.wallNum = i + 1;
            latestWallcolorList.Add(wall);
            Debug.Log("wall : " + wall.wallNum + "Color: " + wall.r + ", " + wall.g + ", " + wall.b);
        }
    }

    public void AddChangesToList(int index, float rVal, float gVal, float bVal)
    {
        if (index == 0)
            return;
        latestWallcolorList[index - 1].isTexture = false;
        latestWallcolorList[index - 1].r = rVal;
        latestWallcolorList[index - 1].g = gVal;
        latestWallcolorList[index - 1].b = bVal;
        //Debug.Log("wall : " + wall.wallNum + "Color: " + wall.r + ", " + wall.g + ", " + wall.b);
    }

    public void AddChangesToList(int index, int textureIndex)
    {
        if (index == 0)
            return;
        latestWallcolorList[index - 1].isTexture = true;
        latestWallcolorList[index - 1].textureIndex = textureIndex;
        //Debug.Log("wall : " + wall.wallNum + "Color: " + wall.r + ", " + wall.g + ", " + wall.b);
    }

    /// <summary>
    /// called when Master sends the RPC data to update list.
    /// </summary>
    /// <param name="wallIndex"></param>
    /// <param name="r"></param>
    /// <param name="g"></param>
    /// <param name="b"></param>
    /// <param name="actorNum"></param>
    [PunRPC]
    public void SyncFromMasterClient(int wallIndex, float r, float g, float b, int actorNum)
    {
        Debug.Log("Getting color : wall :" + wallIndex + ", actorNum : " + actorNum + ", r, g, b : " + r + ", " + g + ", " + b);
        if (PhotonNetwork.LocalPlayer.ActorNumber != actorNum)
        {
            return;
        }
        PaintSync.instance.WallPaintSync(wallIndex, r, g, b);
    }


    [PunRPC]
    public void SyncFromMasterClient_Texture(int wallIndex, int textureIndex, int actorNum)
    {
        Debug.Log("SyncFromMasterClient_Texture : wall :" + wallIndex + ", actorNum : " + actorNum + ", textureIndex : " + textureIndex);
        if (PhotonNetwork.LocalPlayer.ActorNumber != actorNum)
        {
            return;
        }
        //PaintSync.instance.WallPaintSync(wallIndex, r, g, b);
        PaintSync.instance.WallTextureSync(textureIndex, wallIndex);
    }

    /// <summary>
    /// Called when user joins the room.
    /// </summary>
    public void SendUpdateRequest()
    {
        Debug.Log("Sending Update Request 1");
        PV.RPC("SendChangestoPlayer", RpcTarget.MasterClient, PhotonNetwork.LocalPlayer.ActorNumber);
        Debug.Log("Sending Update Request 2");
        Debug.Log("Master Client actor is : " + PhotonNetwork.MasterClient.NickName);
    }

    /// <summary>
    /// IF this is MasterClient, then it executes this function
    /// </summary>
    /// <param name="actorNum"></param>
    [PunRPC]
    public void SendChangestoPlayer(int actorNum)
    {
        if (!PhotonNetwork.IsMasterClient)
            return;
        Debug.Log("SendChangesUpdate on actor num : " + actorNum);
        foreach (PaintSyncDataFormat PS in latestWallcolorList)
        {
            //if(PS.r != 0 || PS.g != 0 || PS.b != 0)
            //{
            //    Debug.Log("SendChangesUpdate Loop on actor num : " + actorNum);
            //    PV.RPC("SyncFromMasterClient", RpcTarget.Others, PS.wallNum, PS.r, PS.g, PS.b, actorNum);
            //}
            if (PS.isTexture)
            {
                PV.RPC("SyncFromMasterClient_Texture", RpcTarget.Others, PS.wallNum, PS.textureIndex, actorNum);
            }
            else if (PS.r != 0 || PS.g != 0 || PS.b != 0)
            {
                Debug.Log("SendChangesUpdate Loop on actor num : " + actorNum);
                PV.RPC("SyncFromMasterClient", RpcTarget.Others, PS.wallNum, PS.r, PS.g, PS.b, actorNum);
            }
        }
    }
}