﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class WallSelector : MonoBehaviour, IPointerClickHandler
{
    public int wallIndex = 0;


    

    public void OnPointerClick(PointerEventData eventData)
    {
        if (Input.touchCount >= 2)
        {
            return;
        }

        PaintSync.instance.selectedWallNumber = wallIndex;
        Debug.Log("Selected Wall Number : " + wallIndex);
        PaintSync.instance.menuCanvas.SetActive(true);
    }
}
