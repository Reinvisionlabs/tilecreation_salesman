﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class CameraSync : MonoBehaviour
{

    public GameObject cameraSecondary;
    PhotonView PV;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    [PunRPC]
    public void CustomerCameraSync(Quaternion rot, int actorNum)
    {

        if(PhotonNetwork.CurrentRoom.GetPlayer(actorNum).NickName != CameraSelector.instance.dDown.options[CameraSelector.instance.dDown.value].text)
        {
            //cameraSecondary.transform.rotation = rot;
            return;
        }

        cameraSecondary.transform.rotation = rot;
    }


}
