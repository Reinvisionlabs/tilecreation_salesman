﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine.UI;

public class MultiPlayerManager : MonoBehaviourPunCallbacks
{
    public static MultiPlayerManager instance;
    public bool isMultiplayerActive = false;

    public CameraSync cameraSync;

    public GameObject panel1;
    //public GameObject panel2;

    private string roomName;

    public string nickName = "Not Assign";
    public InputField inputField;
    public Transform spawnPoint;

    public void AddName()
    {
        if(inputField.text.Length != 0)
        nickName = inputField.text;

        PhotonNetwork.NickName = nickName;
        OnConnectButtonPressed();
    }


    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        //connectButton.SetActive(false);
        PhotonNetwork.ConnectUsingSettings();
        //Debug.Log("Connected to network");
        panel1.SetActive(true);
        //PhotonNetwork.NickName = nickName;

    }



    // Update is called once per frame
    void Update()
    {

    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("Connected to network : " + PhotonNetwork.CloudRegion);
        //connectButton.SetActive(true);
        //CreateRoom();
        
    }

    public void OnConnectButtonPressed()
    {
        //CreateRoom();
        RoomJoiner();

    }

    void RoomJoiner()
    {
        var number = 12345;

        PhotonNetwork.JoinRoom("Tile" + number.ToString());
        Debug.Log("Room number: " + "Tile" + number.ToString() + " RoomJoiner");
    }


    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        Debug.Log("Message : " + message + "Returncode : " + returnCode);
        CreateRoom();
    }

    void CreateRoom()
    {
        //int num = Random.Range(1, 100000);
        int num = 12345;
        RoomOptions roomOp = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = 4, PublishUserId = true };
        PhotonNetwork.CreateRoom("Tile" + num, roomOp);
        roomName = "Tile" + num;
        Debug.Log(roomName);
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("Room Create Failed : " + message + " " + returnCode);
        roomName = " ---- ";
        //CreateRoom();
        RoomJoiner();
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("Room Joined");
        panel1.SetActive(false);
        if (PhotonNetwork.CurrentRoom.PlayerCount >= 2)
            isMultiplayerActive = true;
        // Provide Rooom Name

        CameraSelector.instance.UpdateDropDownOnLateJoin();
        PhotonNetwork.Instantiate("Avatar", spawnPoint.position, Quaternion.identity, 0);

        if (PhotonNetwork.CurrentRoom.PlayerCount >= 2)
        ChangeDataSyncOnNetwork.instance.SendUpdateRequest();

    }

    public override void OnLeftRoom()
    {


    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.Log("newPlayer User ID : " + newPlayer.UserId + ", ActorNumber : " + newPlayer.ActorNumber);

        if (PhotonNetwork.CurrentRoom.PlayerCount >= 2)
            isMultiplayerActive = true;

        CameraSelector.instance.AddDropDownValues(newPlayer);

        Debug.Log("Salesman Joined  = nickname : " + newPlayer.NickName);
        Debug.Log(PhotonNetwork.CurrentRoom.PlayerCount + " Player count");
        Debug.Log(PhotonNetwork.CurrentRoom.Players + " : Players Name");
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        CameraSelector.instance.RemoveDropDownValues(otherPlayer);

        if (PhotonNetwork.CurrentRoom.PlayerCount < 2)
            isMultiplayerActive = false;
        Debug.Log("Salesman Leave = nickname : " + otherPlayer.NickName);
        Debug.Log("otherPlayer User ID : " + otherPlayer.UserId + ", ActorNumber : " + otherPlayer.ActorNumber);


    }
}