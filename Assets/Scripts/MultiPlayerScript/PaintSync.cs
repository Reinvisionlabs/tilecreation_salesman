﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class PaintSync : MonoBehaviour
{

    public static PaintSync instance;

    public int selectedWallNumber;

    public GameObject menuCanvas;


    PhotonView PV;

    public GameObject Wall1;
    public GameObject Wall2;
    public GameObject Wall3;
    public GameObject Wall4;

    GameObject wall;

    //public Shader shader;

    // Start is called before the first frame update
    void Awake()
    {
        instance = this;

    }

    private void Start()
    {
        PV = GetComponent<PhotonView>();
    }


    public void ResetWallIndex()
    {
        selectedWallNumber = 0;
        menuCanvas.SetActive(false);
    }


    public void ChangeColor()
    {

        Color col = ColorSelector.instance.finalColor;
        WallPaintSync(selectedWallNumber, col.r, col.g, col.b);
        PV.RPC("WallPaintSync", RpcTarget.AllBufferedViaServer, selectedWallNumber, col.r, col.g, col.b);
        print("updating color");
        ChangeDataSyncOnNetwork.instance.AddChangesToList(selectedWallNumber, col.r, col.g, col.b);

    }


    [PunRPC]
    public void WallPaintSync(int wallNumber, float r, float g, float b)
    {
        print("Color Values: ");
        //Color col = new Color(r, g, b);
        Debug.Log("WallPaintSync called");

        if (wallNumber == 1)
        {
            Material material1 = new Material(Shader.Find("Standard"));
            Color col = new Color(r, g, b);
            material1.color = col;

            Wall1.GetComponent<WallUI>().isPaintMatActivate = true;
            Wall1.GetComponent<Renderer>().material = material1;
        }

        if (wallNumber == 2)
        {
            Material material2 = new Material(Shader.Find("Standard"));
            Color col = new Color(r, g, b);
            material2.color = col;
            Wall2.GetComponent<WallUI>().isPaintMatActivate = true;
            Wall2.GetComponent<Renderer>().material = material2;
        }

        if (wallNumber == 3)
        {
            Material material3 = new Material(Shader.Find("Standard"));
            Color col = new Color(r, g, b);
            material3.color = col;

            Wall3.GetComponent<WallUI>().isPaintMatActivate = true;
            Wall3.GetComponent<Renderer>().material = material3;
        }

        if (wallNumber == 4)
        {
            Material material4 = new Material(Shader.Find("Standard"));
            Color col = new Color(r, g, b);
            material4.color = col;

            Wall4.GetComponent<WallUI>().isPaintMatActivate = true;
            Wall4.GetComponent<Renderer>().material = material4;
        }

        

        ChangeDataSyncOnNetwork.instance.AddChangesToList(wallNumber, r, g, b);
    }
        

    public void WallTextureClick(int textureIndex)
    {
        if (selectedWallNumber == 0)
            return;

        wall = null;

        WallSelector(selectedWallNumber);

        //Debug.Log("Wall Material is : " + TextureContainer.textureContainer.GetWallDetails(1, 1).name);
        //wall.GetComponent<WallUI>().OnTilePressed(TextureContainer.textureContainer.wallDet_1_3[textureIndex]);


        wall.GetComponent<WallUI>().OnTilePressed(TextureContainer.textureContainer.GetWallDetails(selectedWallNumber, textureIndex));
        //object[] obj = new object[] { selectedWallNumber };

        Debug.Log("Wall TExture Done");

        PV.RPC("WallTextureSync", RpcTarget.Others, textureIndex, selectedWallNumber);

        ChangeDataSyncOnNetwork.instance.AddChangesToList(selectedWallNumber, textureIndex);

    }

    [PunRPC]
    public void WallTextureSync(int textureIndex, int wallSelected)
    {
        //object[] objectArr = (object[])obj;

        //WallDetails select = (WallDetails)objectArr[0];

        //int wallSelected = (int)objectArr[1];

        Debug.Log("wallSelected : " + wallSelected);

        wall = null;

        WallSelector(wallSelected);
              
        wall.GetComponent<WallUI>().OnTilePressed(TextureContainer.textureContainer.GetWallDetails(wallSelected, textureIndex));

        ChangeDataSyncOnNetwork.instance.AddChangesToList(wallSelected, textureIndex);
    }



    public void WallPaintClick()
    {
        if (selectedWallNumber == 0)
            return;

        wall = null;

        WallSelector(selectedWallNumber);

        wall.GetComponent<WallUI>().OnPaintPressed();

        PV.RPC("WallPaintSync", RpcTarget.Others, selectedWallNumber);

    }


    [PunRPC]
    public void WallPaintSync(int wallNum)
    {
        if (wallNum == 0)
            return;

        wall = null;

        WallSelector(wallNum);

        wall.GetComponent<WallUI>().OnPaintPressed();
        
    }


    public void WallSelector(int num)
    {
        switch (num)
        {
            case 1:
                wall = Wall1;
                break;

            case 2:
                wall = Wall2;
                break;

            case 3:
                wall = Wall3;
                break;

            case 4:
                wall = Wall4;
                break;
        }
    }

}
