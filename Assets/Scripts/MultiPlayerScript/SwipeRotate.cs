﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SwipeRotate : MonoBehaviour
{
    public GameObject colorSelector;
    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount == 1 && !colorSelector.activeInHierarchy)
        {
            Debug.Log("Touch");
            // GET TOUCH 0
            Touch touch0 = Input.GetTouch(0);

            // APPLY ROTATION
            if (touch0.phase == TouchPhase.Moved)
            {
                Debug.Log("Touch Phase Moved");
                transform.Rotate(0f, touch0.deltaPosition.x * speed * Time.deltaTime, 0f);
            }

        }

    }
}
