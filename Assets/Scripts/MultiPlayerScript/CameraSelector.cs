﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class CameraSelector : MonoBehaviour
{
    public static CameraSelector instance;
    public Dropdown dDown;

    List<string> userNames;


    // Start is called before the first frame update
    void Start()
    {
        userNames = new List<string>();
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    
    public void AddDropDownValues(Player player)
    {

        userNames.Add(player.NickName);

        dDown.ClearOptions();
        dDown.AddOptions(userNames);

    }

    public void RemoveDropDownValues(Player player)
    {
        userNames.Remove(player.NickName);
        dDown.ClearOptions();
        dDown.AddOptions(userNames);
    }

    public void UpdateDropDownOnLateJoin()
    {
        foreach(Player player in PhotonNetwork.CurrentRoom.Players.Values)
        {
            if(!player.IsLocal)
            {
                userNames.Add(player.NickName);
            }
        }

        dDown.ClearOptions();
        dDown.AddOptions(userNames);
    }
}
