﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudTilesManager : MonoBehaviour
{
    [SerializeField]
    Transform wallTileListContent;

    [SerializeField]
    GameObject tilePrefab;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            Instantiate(tilePrefab, wallTileListContent);
        }
    }
}
