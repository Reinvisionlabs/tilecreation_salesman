﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    public Transform raycastOrigin;
    public float rayDis;
    public GameObject line;
    public GameObject teleportMark;
    public GameObject player;
    public float offset;

    LineRenderer lineR;
    public RaycastHit hit;

    // Start is called before the first frame update
    void Awake()
    {
        teleportMark.SetActive(false);
        line.SetActive(false);
        lineR = line.GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (OVRInput.Get(OVRInput.Button.SecondaryThumbstickUp))
        {
            line.SetActive(true);

            if (Physics.Raycast(raycastOrigin.position, raycastOrigin.forward, out hit, rayDis))
            {

                lineR.SetPosition(0, raycastOrigin.position);
                lineR.SetPosition(1, hit.point);

                if (!hit.transform.gameObject.CompareTag("floor"))
                {
                    teleportMark.SetActive(false);
                    return;
                }

                Vector3 markPos = new Vector3(hit.point.x, hit.point.y + offset, hit.point.z);
                                
                teleportMark.transform.position = markPos;
                teleportMark.SetActive(true);


            }

            else
            {
                lineR.SetPosition(0, raycastOrigin.position);
                lineR.SetPosition(1, raycastOrigin.forward + new Vector3(0, 0, rayDis));
                teleportMark.SetActive(false);
            }

        }

        if (OVRInput.GetUp(OVRInput.Button.SecondaryThumbstickUp))
        {
            teleportMark.SetActive(false);
            line.SetActive(false);

            if (!hit.transform.gameObject.CompareTag("floor")) return;

            Vector3 pos = new Vector3(hit.point.x, player.transform.position.y, hit.point.z);

            player.transform.position = pos;

        }
        

    }
}
