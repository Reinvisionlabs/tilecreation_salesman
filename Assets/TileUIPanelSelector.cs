﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileUIPanelSelector : MonoBehaviour
{

    public GameObject scrollTileOne;
    public GameObject scrollTileTwo;

    public void UI_Selector()
    {
        if(PaintSync.instance.selectedWallNumber == 1 || PaintSync.instance.selectedWallNumber == 3)
        {
            scrollTileOne.SetActive(true);
            scrollTileTwo.SetActive(false);
        }

        else
        {
            scrollTileOne.SetActive(false);
            scrollTileTwo.SetActive(true);
        }
    }


}
