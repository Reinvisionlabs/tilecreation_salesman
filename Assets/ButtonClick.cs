﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonClick : MonoBehaviour
{

    public void Pressed()
    {
        Debug.Log("Pressed");
        transform.Rotate(Vector3.up, 20f);
    }

    public void Disabled()
    {
        Debug.Log("Disabled");
        transform.Rotate(Vector3.up, -20f);
    }

}
