﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientCamera : MonoBehaviour
{

    public GameObject obj;

    public void CameraToggle()
    {
        if(obj.activeInHierarchy)
        {
            obj.SetActive(false);
        }

        else
        {
            obj.SetActive(true);
        }
    }

}
