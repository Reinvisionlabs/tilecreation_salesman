﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallInfo : MonoBehaviour {

    // universal grid scale
    public float gridScale = 1f;

    // extents of the grid
    public float minX = -15;
    public float minY = -15;
    public float maxX = 15;
    public float maxY = 15;

    // nudges the whole grid rel
    public Vector3 gridOffset = Vector3.zero;

    // choose a colour for the gizmos
    public int gizmoMajorLines = 5;
    public Color gizmoLineColor = new Color(0.4f, 0.4f, 0.3f, 1f);

    [Range(0.1f, 100)]
    public float cellHeight = 1;
    [Range(0.1f, 100)]
    public float cellWidth = 1;

    public float wallHeight;
    public float wallWidth;

    // rename + centre the gameobject upon first time dragging the script into the editor. 
    void Reset()
    {
        if (name == "GameObject")
            name = "~~ GIZMO GRID ~~";

        transform.position = Vector3.zero;
    }

    // draw the grid :) 
    void OnDrawGizmos()
    {

        wallHeight = (maxX - minX);
        wallWidth = (maxY - minY);

        // orient to the gameobject, so you can rotate the grid independently if desired
        Gizmos.matrix = transform.localToWorldMatrix;

        // set colours
        Color dimColor = new Color(gizmoLineColor.r, gizmoLineColor.g, gizmoLineColor.b, 0.25f * gizmoLineColor.a);
        Color brightColor = Color.Lerp(Color.white, gizmoLineColor, 0.75f);

        // draw the horizontal lines
        for (float x = minX; x < maxX; x+= cellHeight)
        {
            // find major lines
            Gizmos.color = (x % gizmoMajorLines == 0 ? gizmoLineColor : dimColor);
            if (x == 0)
                Gizmos.color = brightColor;

            Vector3 pos1 = new Vector3(x, 0, minY) * gridScale;
            Vector3 pos2 = new Vector3(x, 0, maxY) * gridScale;

            Gizmos.DrawLine((gridOffset + pos1), (gridOffset + pos2));
        }

        Vector3 hLast1 = new Vector3(maxX, 0, minY) * gridScale;
        Vector3 hLast2 = new Vector3(maxX, 0, maxY) * gridScale;

        Gizmos.DrawLine((gridOffset + hLast1), (gridOffset + hLast2));

        // draw the vertical lines
        for (float y = minY; y < maxY; y+= cellWidth)
        {
            // find major lines
            Gizmos.color = (y % gizmoMajorLines == 0 ? gizmoLineColor : dimColor);
            if (y == 0)
                Gizmos.color = brightColor;

            Vector3 pos1 = new Vector3(minX, 0, y) * gridScale;
            Vector3 pos2 = new Vector3(maxX, 0, y) * gridScale;

            Gizmos.DrawLine((gridOffset + pos1), (gridOffset + pos2));
        }

        Vector3 vLast1 = new Vector3(minX, 0, maxY) * gridScale;
        Vector3 vLast2 = new Vector3(maxX, 0, maxY) * gridScale;

        Gizmos.DrawLine((gridOffset + vLast1), (gridOffset + vLast2));
    }

    public float GetHorizontalCount()
    {
        return (maxX - minX) / cellHeight;
    }

    public float GetVerticalCount()
    {
        return (maxY - minY) / cellWidth;
    }
}
