﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileGrid : MonoBehaviour
{

    // universal grid scale
    public float gridScale = 1f;

    // extents of the grid
    public float height = 5;
    public float width = 5;

    // choose a colour for the gizmos
    public Color gizmoLineColor = new Color(0.4f, 0.4f, 0.3f, 1f);

    // nudges the whole grid rel
    public Vector3 gridOffset = Vector3.zero;

    //The material for reference
    public Material mainMaterial;

    private Material[] cellMaterials;

    [Range(0.1f, 100)]
    public float cellHeight = 1;
    [Range(0.1f, 100)]
    public float cellWidth = 1;

    // extents of the grid
    private float minX = -15;
    private float minY = -15;
    private float maxX = 15;
    private float maxY = 15;

    private bool useRandom = false;

    private List<GameObject> createdTiles = new List<GameObject>();

    // Draw the grid :) 
    void OnDrawGizmos()
    {
        //Calculate the minimum/maximum x and z

        minX = -height / 2;
        minY = -width / 2;

        maxX = height / 2;
        maxY = width / 2;

        // orient to the gameobject, so you can rotate the grid independently if desired
        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.color = gizmoLineColor;

        // draw the horizontal lines
        for (float x = minX; x < maxX + cellHeight; x+= cellHeight)
        {
            Vector3 pos1 = new Vector3(x, 0, minY) * gridScale;
            Vector3 pos2 = new Vector3(x, 0, maxY) * gridScale;

            Gizmos.DrawLine((gridOffset + pos1), (gridOffset + pos2));
        }

        // draw the vertical lines
        for (float y = minY; y < maxY + cellWidth; y+= cellWidth)
        {
            Vector3 pos1 = new Vector3(minX, 0, y) * gridScale;
            Vector3 pos2 = new Vector3(maxX, 0, y) * gridScale;

            Gizmos.DrawLine((gridOffset + pos1), (gridOffset + pos2));
        }
    }

    public void Reset()
    {
        foreach ( GameObject item in createdTiles)
        {
            if(item != null)
            {
                Destroy(item);
            }
        }
        createdTiles.Clear();
    }

    public void CreateTilesInternal(WallDetails wallDetails)
    {
        minX = -height / 2;
        minY = -width / 2;

        maxX = height / 2;
        maxY = width / 2;

        int i = 0, j = 0;

        // draw the horizontal lines
        for (float x = minX; x < maxX; x += cellHeight)
        {
            j = 0;
            for (float y = minY; y < maxY; y += cellWidth)
            {
                Vector3 pos1 = new Vector3(x, 0, y) * gridScale;

                if(wallDetails.style == WallDetails.Style.Random)
                {
                    createdTiles.Add(CreateQuad(string.Format("Tile {0} - {1}", x, y), pos1, cellMaterials[Random.Range(0, cellMaterials.Length)]));
                }
                else if (wallDetails.style == WallDetails.Style.Grid)
                {
                    createdTiles.Add(CreateQuad(string.Format("Tile {0} - {1}", x, y), pos1, cellMaterials[(i + j) % cellMaterials.Length]));
                }
                else if (wallDetails.style == WallDetails.Style.Horizontal)
                {
                    createdTiles.Add(CreateQuad(string.Format("Tile {0} - {1}", x, y), pos1, cellMaterials[j % cellMaterials.Length]));
                }
                else if (wallDetails.style == WallDetails.Style.Vertical)
                {
                    createdTiles.Add(CreateQuad(string.Format("Tile {0} - {1}", x, y), pos1, cellMaterials[i % cellMaterials.Length]));
                }
                else if (wallDetails.style == WallDetails.Style.None)
                {
                    createdTiles.Add(CreateQuad(string.Format("Tile {0} - {1}", x, y), pos1, cellMaterials[0]));
                }
                j++;
            }
            i++;
        }


    }

    public void CreateTiles(WallDetails wallDetails)
    {
        //Set the values
        cellMaterials = wallDetails.materials;
        //cellHeight = height / wallDetails.GetHorizontalTileCount();
        //cellWidth = width / wallDetails.GetVerticalTileCount();

        cellWidth = height / wallDetails.GetHorizontalTileCount();
        cellHeight = width / wallDetails.GetVerticalTileCount();
        useRandom = wallDetails.isRandom;

        //Create the tile
        CreateTilesInternal(wallDetails);
    }

    private GameObject CreateQuad(string quadName = "Quad", Vector3 pos = default(Vector3), Material material = null)
    {
        //Debug.Log("Creating a quad");
        GameObject quad = new GameObject(quadName);
        quad.transform.parent = transform;
        quad.transform.localRotation = Quaternion.identity;
        quad.layer = transform.gameObject.layer;
        //if(pos != default(Vector3))
        quad.transform.localPosition = pos;
        quad.AddComponent<MeshRenderer>().material = material;
        Mesh mesh =  quad.AddComponent<MeshFilter>().mesh;


        //Create vertices array
        Vector3[] vertices = new Vector3[4];

        vertices[0] = new Vector3(0, 0, 0);
        vertices[1] = new Vector3(cellHeight, 0, 0);
        vertices[2] = new Vector3(0, 0, cellWidth);
        vertices[3] = new Vector3(cellHeight, 0, cellWidth);

        mesh.vertices = vertices;

        //Create the tris
        int[] tri= new int[6];

        //  Lower left triangle.
        tri[0] = 0;
        tri[1] = 2;
        tri[2] = 1;

        //  Upper right triangle.   
        tri[3] = 2;
        tri[4] = 3;
        tri[5] = 1;

        mesh.triangles = tri;

        //Create the normals
        Vector3[] normals = new Vector3[4];

        normals[0] = -Vector3.forward;
        normals[1] = -Vector3.forward;
        normals[2] = -Vector3.forward;
        normals[3] = -Vector3.forward;

        mesh.normals = normals;

        //Create UV map array
        Vector2[] uv = new Vector2[4];

        uv[0] = new Vector2(0, 0);
        uv[1] = new Vector2(1, 0);
        uv[2] = new Vector2(0, 1);
        uv[3] = new Vector2(1, 1);

        mesh.uv = uv;

        return quad;
    }
}
