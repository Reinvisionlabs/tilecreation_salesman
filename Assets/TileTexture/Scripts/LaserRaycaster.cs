﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserRaycaster : MonoBehaviour
{
    [SerializeField]
    float distance;

    private void Update()
    {
        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, distance))
        {
            if (hit.transform.gameObject.layer.Equals("UI"))
            {
                Debug.Log("UI Trackedddd");
            }
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);

            //Debug.Log("Did Hit");
        }
        else
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * distance, Color.red);
            Debug.Log("Did not Hit");
        }
    }
}
