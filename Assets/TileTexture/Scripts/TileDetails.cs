﻿using UnityEngine;

[System.Serializable]
public class TileDetails
{
    public string tileName;
    public Texture[] tileTextures;
    public float tileHeight = 1;
    public float tileWidth = 1;
}
