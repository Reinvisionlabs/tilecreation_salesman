﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TileGrid))]
public class TileGridHelper : MonoBehaviour {

    private TileGrid tileGrid;
    private Camera tileCamera;

    public WallDetails[] wallDetailsList;

    private int index = 0;

	// Use this for initialization
	void Start () {
        tileGrid = GetComponent<TileGrid>();
        tileCamera = GetComponentInChildren<Camera>();
        //CreateTexture(wallDetailsList[12]);
    }

	
	// Update is called once per frame
	void Update () {
        //if (Input.GetKeyDown(KeyCode.T))
        //{
        //    CreateTexture(wallDetailsList[index]);
        //    index++;
        //    if (index >= wallDetailsList.Length)
        //        index = 0;
        //}
	}

    public void CreateTexture(WallDetails wallDetails)
    {

        Debug.Log("Tile Creation");
        //Remove existing render texture from camera
        tileCamera.targetTexture = null;

        //Clear excisting tiles
        tileGrid.Reset();

        //Set the materials and textures for the tiles
        //Tile configuration (maybe later)
        //Create the tile
        tileGrid.CreateTiles(wallDetails);

        //Set the Rendertexture
        tileCamera.targetTexture = wallDetails.texture;

        //Enable the camera
        tileCamera.enabled = true;

    }

    public void OnButtonPressed(int index)
    {
        Debug.Log("Pressed");
        CreateTexture(wallDetailsList[index]);
    }

    public void OnButtonPressed(WallDetails selected)
    {
        Debug.Log("Pressed");
        CreateTexture(selected);
    }

}
