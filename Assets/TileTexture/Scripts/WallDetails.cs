﻿using UnityEngine;

[CreateAssetMenu(fileName = "Wall Details 0", menuName = "Data/WallDetails", order = 1)]
public class WallDetails : ScriptableObject{

    public enum Style{None, Random, Grid, Vertical, Horizontal}
    public string wallName = "Wall";
    [Space(10)]
    public Material[] materials;
    public RenderTexture texture;
    public Style style = Style.None;
    public bool isRandom = false;

    [Header("Wall Size")]
    [Range(0.1f,100)]
    public float wallHeight = 1;
    [Range(0.1f, 100)]
    public float wallWidth = 1;

    [Header("Tile Size")]
    [Range(0.1f, 100)]
    public float tileHeight = 1;
    [Range(0.1f, 100)]
    public float tileWidth = 1;

    public float GetHorizontalTileCount()
    {
        return wallHeight / tileHeight;
    }
    public float GetVerticalTileCount()
    {
        return wallWidth / tileWidth;
    }

}
