﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class WallUI : MonoBehaviour {

    public GameObject wallObjet;
    public RenderTexture wallTexture;
    //public ColorSelector colorSelector;

    //public WallDetails defaultSelected;
    //public UnityEngine.UI.RawImage rawImage;
    //public WallDetails tempSelected;
    private TileGridHelper tileGridHelper;
    private WallDetails lastSelected;
    private WallDetails lastPattern;

    WallDetails.Style patternStyle = WallDetails.Style.Grid;

    public Vector2 tiling;
    private Material wallMaterial;
    private Material lastTileMaterial;

    bool initialStage = false;
    public bool isPaintMatActivate = false;
    Material paintMaterial;
    public bool isMultiplayerActive = false;

    //public GameObject multiplayerManager;
    


    private void Awake()
    {
        wallTexture = new RenderTexture(2048, 2048, 1);
        //wallTexture = (RenderTexture) wallObjet.GetComponent<Renderer>().material.mainTexture;
        tileGridHelper = FindObjectOfType<TileGridHelper>();

        wallMaterial = wallObjet.GetComponent<Renderer>().material;
        lastTileMaterial = wallMaterial;

        tiling = wallMaterial.mainTextureScale;
        //initialStage = true;

       
    }

    /*private void OnEnable()
    {
        MultiPlayerManager.MultiplayerStateEnable += OnMultiplayerObject;
    }*/

    //private void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.X))
    //    {
    //        OnPreLoaded();
    //    }else if (Input.GetKeyDown(KeyCode.V))
    //    {
    //        OnPatternPressed(tempSelected);
    //    }
    //}

    //public void OnPreLoaded()
    //{
    //    if (initialStage)
    //    {
    //        defaultSelected.texture = wallTexture;
    //        tileGridHelper.CreateTexture(defaultSelected);
    //        lastSelected = defaultSelected;
    //        lastSelected.texture = wallTexture;
    //        rawImage.texture = wallTexture;
    //        //initialStage = false;
    //    }            
    //}


    public void OnExpandButtonClicked(GameObject button)
    {
        button.SetActive(!button.activeSelf);
    }

   
    public void OnTilePressed(WallDetails selected)
    {
        Debug.Log("Tile Pressed"); 
        
        if(isPaintMatActivate)
        {
            wallMaterial = lastTileMaterial;
            wallObjet.GetComponent<Renderer>().material = lastTileMaterial;
            isPaintMatActivate = false;
        }

        selected.texture = wallTexture;
        tileGridHelper.CreateTexture(selected);
        wallMaterial.mainTexture = wallTexture;
        //wallMaterial.SetTextureScale("_MainTex", Vector2.one);
        wallMaterial.mainTextureScale = Vector2.one;
        lastSelected = selected;
        ///CreateTexture(selected);
    }

   
    public void OnPaintPressed()
    {
        Debug.Log("Paint Pressed");
        if(!isPaintMatActivate)
        {
            lastTileMaterial = wallMaterial;
            isPaintMatActivate = true;
        }

        //wallObjet.GetComponent<Renderer>().material = mat;
        

    }


    public void OnColorPicker(Material mat)
    {
        if (!isPaintMatActivate)
        {
            isPaintMatActivate = true;
            lastTileMaterial = wallMaterial;
        }
        paintMaterial = mat;
        wallObjet.GetComponent<Renderer>().material = paintMaterial;
        //paintMaterial.color = menuCanvas.finalColor;

    }


    private void Update()
    {
        //paintMaterial.color = menuCanvas.finalColor;
    }

    public void OnPatternPressed(WallDetails selected)
    {
        Debug.Log("Tile Pattern");
        if(lastSelected != null)
            selected.materials[0] = lastSelected.materials[0];
        selected.style = patternStyle;
        selected.texture = wallTexture;
        tileGridHelper.CreateTexture(selected);
        //rawImage.texture = wallTexture;
        lastPattern = selected;
        ///CreateTexture(selected);
    }

    public void OnStylePressed(int status)
    {
        Debug.Log("Tile Pattern");

        if (status == 0)
            patternStyle = WallDetails.Style.None;
        if (status == 1)
            patternStyle = WallDetails.Style.Grid;
        if (status == 2)
            patternStyle = WallDetails.Style.Vertical;
        if (status == 3)
            patternStyle = WallDetails.Style.Horizontal;


        if (lastSelected != null && lastPattern != null)
        {
            lastPattern.materials[0] = lastSelected.materials[0];
            lastPattern.style = patternStyle;
            lastPattern.texture = wallTexture;
            tileGridHelper.CreateTexture(lastPattern);
        }
        ///CreateTexture(selected);
    }

    private void OnDestroy()
    {
        initialStage = true;
        if(wallMaterial != null)
        {
            wallMaterial.mainTextureScale = tiling;
        }
    }
}
