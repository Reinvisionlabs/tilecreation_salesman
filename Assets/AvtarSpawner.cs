﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AvtarSpawner : MonoBehaviour
{
    public Text name;
    public GameObject player;
    PhotonView PV;

    bool isPlayerSync = false;
    
    // Start is called before the first frame update
    void Start()
    {
        PV = GetComponent<PhotonView>();
    }

    private void OnEnable()
    {
       //name.text = PV.Owner.NickName;
        Debug.Log("Name");
        
        Invoke("AddName", 5f);

    }
    
    public void AddName()
    {

        name.text = PV.Owner.NickName;
        Debug.Log("Name is ::" + PV.Owner.NickName);

        if (PV.IsMine)
        {
            isPlayerSync = true;
            player.SetActive(false);
        }
    }

    private void OnDisable()
    {
        name.text = null;
        isPlayerSync = false;
    }

    private void Update()
    {
        if (isPlayerSync)
        {
            Vector3 rot = new Vector3(transform.rotation.eulerAngles.x, Camera.main.transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
            transform.rotation = Quaternion.Euler(rot);
        }
    }
}




